package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid currGen;

    public BriansBrain(int rows, int columns){
        currGen = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState (int row, int column) {
        return currGen.get(row, column);
    }

    @Override
    public void initializeCells () {
        Random random = new Random();
        for (int row = 0; row < currGen.numRows(); row++) {
            for (int col = 0; col < currGen.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currGen.set(row, col, CellState.ALIVE);
                } else {
                    currGen.set(row, col, CellState.DEAD);
                }
            }
        }
    }



    @Override
    public void step () {
    IGrid nextGen = currGen.copy();
    for(int row = 0; row<nextGen.numRows();row++){
        for (int col =0; col<nextGen.numColumns(); col++){
            nextGen.set(row,col,getNextCell(row,col));
        }
    }
    this.currGen = nextGen;

    }

    @Override
    public CellState getNextCell (int row, int col) {
        if(this.currGen.get(row, col).equals(CellState.ALIVE)) {
            return CellState.DYING;
        }else if(this.currGen.get(row, col).equals(CellState.DYING)){
            return CellState.DEAD;
        }else if(this.currGen.get(row, col).equals(CellState.DEAD)) {
            if(countNeighbors(row,col,CellState.ALIVE)==2) {
                return CellState.ALIVE;
            }
        }
        return CellState.DEAD;

    }

    private int countNeighbors(int row, int col, CellState state){
    int neighborscount = 0;
        int posNeighbor[][] = {{1,1}, {-1,-1}, {-1,1}, {1,-1}, {1,0}, {0,1}, {-1,0}, {0,-1}};
        for (int i = 0; i < 8; i++){
            try {
                if(this.currGen.get(row + posNeighbor[i][0], col + posNeighbor[i][1]).equals(state)) {
                    neighborscount+=1;
                }
            }
            catch(IndexOutOfBoundsException e) { }
        }
        return neighborscount;
    }



    @Override
    public int numberOfRows () {
        return this.currGen.numRows();
    }

    @Override
    public int numberOfColumns () {
        return  this.currGen.numColumns();
    }

    @Override
    public IGrid getGrid () {
        return currGen;
    }
}
