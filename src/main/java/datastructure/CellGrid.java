package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private final int rows;
    private final int cols;
    private final CellState[][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows=rows;
        this.cols=columns;
        grid = new CellState[rows][columns];

    }
    public boolean checkLimits(int row, int column) {
        return row >= 0 && row < numRows() && column >= 0 && column < numColumns();
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.cols;
    }


    @Override
    public void set(int row, int column, CellState element) {
        if (checkLimits(row, column)) {
            grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (checkLimits(row, column)) {
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }



    @Override
    public IGrid copy() {
        IGrid gridCopy  = new CellGrid(numRows(),numColumns(),CellState.DEAD);
        for (int row = 0; row < numRows(); row++) {
            for (int column = 0; column < numColumns(); column++) {
                gridCopy .set(row, column, get(row, column));
            }
        }
        return gridCopy ;
    }

}
    

